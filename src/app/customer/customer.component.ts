import { Component, OnInit, Input } from '@angular/core';
import { Customer } from './customer';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit {

  constructor() { }
  customers:Customer[]=[]
  selectedCustomer:Customer;
  @Input() city:string;
  ngOnInit() {
    this.customers = [
      {id:1,firstName:"Samet",lastName:"Uca",age:24},
      {id:2,firstName:"Sam",lastName:"Uca",age:25},
      {id:3,firstName:"et",lastName:"Uca",age:26}

    ]
  }
  selectCustomer(customer:Customer){
    this.selectedCustomer = customer;
  }

}
