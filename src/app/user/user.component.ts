import { Component, OnInit } from '@angular/core';
import { User } from './user';
import { HttpClientModule, HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  constructor(private http:HttpClient) { }
  user:User[];
  path:string="https://jsonplaceholder.typicode.com/users"
  ngOnInit() {
    this.getUsers();
  }
  getUsers(){
    this.http.get<User[]>(this.path).subscribe(response=>{
      this.user = response;
    })
  }
}
